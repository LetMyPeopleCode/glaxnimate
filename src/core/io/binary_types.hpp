#pragma once

#include <cstdint>

namespace glaxnimate::io {

using VarUint = std::uint64_t;
using Float32 = float;

} // namespace glaxnimate::io
